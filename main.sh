#!/bin/bash

# Ativa o ambiente virtual python
source venv/bin/activate

# Considerando o ambiente virtual ativado, chama o script
python3 main.py "$@"

# Sai do ambiente virtual
deactivate
