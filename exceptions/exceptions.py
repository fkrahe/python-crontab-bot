class TimeControlException(Exception):
    """
    This exception informs that we called the script too soon after the last execution.
    This means that we shall not proceed. We must stop the execution.
    """
    pass
