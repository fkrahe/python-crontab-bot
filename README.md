# How to use

## Setup python environment

In your projects folder, create your _venv_ folder

```shell script
$ python3 -m venv venv
$ source venv/bin/activate
(venv) $ pip install -r requirements.txt
```

After installing something new with ```$ pip install```, run ```$ pip freeze > requirements.txt``` to update it.

## Setup your dot env file

Create dot env file and modify the minimum time between consecutive runs,
so you can call your script every minute without worries.

```shell script
$ cp env_example .env
$ vi .env
```

Set the correct value (in seconds) for CONTROL_WINDOW_SEC.

## Setup crontab call 

Edit your user's crontab and place a recurrent call every minute.
Use the dot env file to control the real interval, with CONTROL_WINDOW_SEC.

```shell script
$ contrab -e

## Calling my script
* * * * * cd /home/<user>/project_folder && /bin/bash main.sh >> /dev/null 2>&1
```