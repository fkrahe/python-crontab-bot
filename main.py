import logging
import os

from dotenv import load_dotenv
from logging.handlers import RotatingFileHandler

from botclasses.basebot import BaseBot

# Load environment vars
load_dotenv()
CONTROL_WINDOW_SEC = int(os.getenv('CONTROL_WINDOW_SEC'))

# Setup log
if not os.path.isdir('logs'):
    os.makedirs('logs')

logging.basicConfig(
    format='%(asctime)s %(message)s',
    level=logging.DEBUG,
    handlers=[
        RotatingFileHandler(
            filename='logs/bot.log',
            backupCount=7
        ),
        logging.StreamHandler()
    ]
)

# Call our bot
if __name__ == "__main__":
    bot = BaseBot(CONTROL_WINDOW_SEC)
    bot.run()
