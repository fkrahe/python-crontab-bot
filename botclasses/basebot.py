import datetime
import logging

from pathlib import Path

from exceptions.exceptions import TimeControlException


class BaseBot:

    def __init__(self, control_window_sec=300):
        self.control_file = 'control.txt'
        self.control_window_sec = control_window_sec

    def __control_file_touch(self):
        logging.debug('Touching the control file.')
        Path(self.control_file).touch()

    def __control_file_test(self):
        """
        Will test if the control file modification time
        is too recent (now-mtime<=control_window_sec) or
        if we can proceed
        :return:
        """
        controle = Path(self.control_file)
        if not controle.exists():
            logging.debug('The {} file does not exist, creating.'.format(self.control_file))
            self.__control_file_touch()
        else:
            logging.debug('The {} file does exist, checking modified time.'.format(self.control_file))

            controle_mtime = controle.stat().st_mtime
            mtime = datetime.datetime.fromtimestamp(controle_mtime)
            now = datetime.datetime.now()
            diff_s = (now - mtime).seconds
            if diff_s <= self.control_window_sec:
                logging.debug('Last modification {} sec ago, leaving. Current window is {} sec.'.format(
                    diff_s,
                    self.control_window_sec
                ))
                raise TimeControlException()
            else:
                logging.debug('We can proceed.')

    def run(self):
        logging.debug('START')

        try:
            self.__control_file_test()
        except TimeControlException:
            quit()

        # Execute your code
        # ...

        logging.warning('Implement your code here')

        # ...
        # After executing some task that requires a waiting time,
        # touch control file, so we can start to count that time.
        self.__control_file_touch()

        logging.debug('END')
